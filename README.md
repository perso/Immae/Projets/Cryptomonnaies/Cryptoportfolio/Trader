# Sell all

    import portfolio
    portfolio.Balance.prepare_trades_to_sell_all(portfolio.market)
    
    portfolio.Trade.prepare_orders(compute_value=lambda x, y: x["bid"] * portfolio.D("1.001"))
    
    portfolio.Trade.print_all_with_order()
    
    portfolio.Trade.run_orders()


# get balance:

    portfolio.Balance.fetch_balance(portfolio.market)

# follow orders:

    portfolio.Trade.follow_orders(sleep=3)

# open orders:

    portfolio.Trade.all_orders(state="open")


# Buy
