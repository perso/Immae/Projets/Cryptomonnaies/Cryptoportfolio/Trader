install:
ifeq (${VIRTUAL_ENV},)
	pip install --user -r requirements.txt
else
	pip install -r requirements.txt
endif

test:
	python test.py

test_unit:
	python test.py --onlyunit

test_acceptance:
	python test.py --onlyacceptance

run:
	python portfolio.py

release_version = $(shell git describe --tags --always)
release_file = trader_$(release_version).tar.gz
folder = ~/.no_backup/projets/git.immae.eu/releases/cryptoportfolio/trader

coverage_omit = "tests/*.py,test.py"

build_release:
	git archive HEAD -o $(release_file)
	mv $(release_file) $(folder)
	@echo "====== Version information ======"
	@echo "$(release_version)"
	@echo -n "sha256: "
	@sha256sum $(folder)/$(release_file) | cut -d' ' -f1
	@echo "================================="

test_coverage:
	coverage run --source=. --omit="$(coverage_omit)" test.py
	coverage report -m

test_coverage_html: test_coverage
	coverage html
	rm ~/hosts/www.immae.eu/htmlcov -rf && cp -r htmlcov ~/hosts/www.immae.eu
	@echo "coverage in https://www.immae.eu/htmlcov"

test_coverage_unit:
	coverage run --source=. --omit="$(coverage_omit)" test.py --onlyunit
	coverage report -m

test_coverage_unit_html: test_coverage_unit
	coverage html
	rm ~/hosts/www.immae.eu/htmlcov -rf && cp -r htmlcov ~/hosts/www.immae.eu
	@echo "coverage in https://www.immae.eu/htmlcov"

test_coverage_acceptance:
	coverage run --source=. --omit="$(coverage_omit)" test.py --onlyacceptance
	coverage report -m

test_coverage_acceptance_html: test_coverage_acceptance
	coverage html
	rm ~/hosts/www.immae.eu/htmlcov -rf && cp -r htmlcov ~/hosts/www.immae.eu
	@echo "coverage in https://www.immae.eu/htmlcov"
