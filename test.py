import unittest
from tests.acceptance import TimeMock

from tests.helper import limits

if "unit" in limits:
    from tests.test_ccxt_wrapper import *
    from tests.test_main import *
    from tests.test_market import *
    from tests.test_store import *
    from tests.test_portfolio import *
    from tests.test_dbs import *

if "acceptance" in limits:
    from tests.test_acceptance import *

if __name__ == '__main__':
    unittest.main()
