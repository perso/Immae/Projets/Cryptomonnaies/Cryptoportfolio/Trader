from ccxt import *
import decimal
import time
from retry.api import retry_call
import re
from requests.exceptions import RequestException
from ssl import SSLError

def _cw_exchange_sum(self, *args):
    return sum([arg for arg in args if isinstance(arg, (float, int, decimal.Decimal))])
Exchange.sum = _cw_exchange_sum

class poloniexE(poloniex):
    RETRIABLE_CALLS = [
            re.compile(r"^return"),
            re.compile(r"^cancel"),
            re.compile(r"^closeMarginPosition$"),
            re.compile(r"^getMarginPosition$"),
            ]

    def request(self, path, api='public', method='GET', params={}, headers=None, body=None):
        """
        Wrapped to allow retry of non-posting requests"
        """

        origin_request = super().request
        kwargs = {
                "api": api,
                "method": method,
                "params": params,
                "headers": headers,
                "body": body
                }

        retriable = any(re.match(call, path) for call in self.RETRIABLE_CALLS)
        if api == "public" or method == "GET" or retriable:
            return retry_call(origin_request, fargs=[path], fkwargs=kwargs,
                    tries=10, delay=1, exceptions=(RequestTimeout, InvalidNonce))
        else:
            return origin_request(path, **kwargs)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # For requests logging
        self.session.origin_request = self.session.request
        self.session._parent = self

        def request_wrap(self, *args, **kwargs):
            kwargs["headers"]["X-market-id"] = str(self._parent._market.market_id)
            kwargs["headers"]["X-user-id"] = str(self._parent._market.user_id)
            try:
                r = self.origin_request(*args, **kwargs)
                self._parent._market.report.log_http_request(args[0],
                        args[1], kwargs["data"], kwargs["headers"], r)
                return r
            except (SSLError, RequestException) as e:
                self._parent._market.report.log_http_request(args[0],
                        args[1], kwargs["data"], kwargs["headers"], e)
                raise e

        self.session.request = request_wrap.__get__(self.session,
                self.session.__class__)

    @staticmethod
    def nanoseconds():
        return int(time.time() * 1000000000)

    def is_dust_trade(self, amount, rate):
        if abs(amount) < decimal.Decimal("0.000001"):
            return True
        if abs(amount * rate) < decimal.Decimal("0.0001"):
            return True
        return False

    def fetch_margin_balance(self):
        """
        portfolio.market.privatePostGetMarginPosition({"currencyPair": "BTC_DASH"})
        See DASH/BTC positions
        {'amount': '-0.10000000',         -> DASH empruntés (à rendre)
        'basePrice': '0.06818560',        -> à ce prix là (0.06828800 demandé * (1-0.15%))
        'lendingFees': '0.00000000',      -> ce que je dois à mon créditeur en intérêts
        'liquidationPrice': '0.15107132', -> prix auquel ça sera liquidé (dépend de ce que j’ai déjà sur mon compte margin)
        'pl': '-0.00000371',              -> plus-value latente si je rachète tout de suite (négatif = perdu)
        'total': '0.00681856',            -> valeur totale empruntée en BTC (au moment de l'échange)
                                             = amount * basePrice à erreur d'arrondi près
        'type': 'short'}
        """
        positions = self.privatePostGetMarginPosition({"currencyPair": "all"})
        parsed = {}
        for symbol, position in positions.items():
            if position["type"] == "none":
                continue
            base_currency, currency = symbol.split("_")
            parsed[currency] = {
                    "amount": decimal.Decimal(position["amount"]),
                    "borrowedPrice": decimal.Decimal(position["basePrice"]),
                    "lendingFees": decimal.Decimal(position["lendingFees"]),
                    "pl": decimal.Decimal(position["pl"]),
                    "liquidationPrice": decimal.Decimal(position["liquidationPrice"]),
                    "type": position["type"],
                    "total": decimal.Decimal(position["total"]),
                    "baseCurrency": base_currency, 
                    }
        return parsed

    def fetch_balance_per_type(self):
        balances = self.privatePostReturnAvailableAccountBalances()
        result = {'info': balances}
        for key, balance in balances.items():
            result[key] = {}
            for currency, amount in balance.items():
                result.setdefault(currency, {})
                result[currency][key] = decimal.Decimal(amount)
                result[key][currency] = decimal.Decimal(amount)
        return result

    def fetch_all_balances(self):
        exchange_balances = self.fetch_balance()
        margin_balances = self.fetch_margin_balance()
        balances_per_type = self.fetch_balance_per_type()

        all_balances = {}
        in_positions = {}
        pending_pl = {}

        for currency, exchange_balance in exchange_balances.items():
            if currency in ["info", "free", "used", "total"]:
                continue

            margin_balance = margin_balances.get(currency, {})
            balance_per_type = balances_per_type.get(currency, {})

            all_balances[currency] = {
                "total": exchange_balance["total"] + margin_balance.get("amount", 0),
                "exchange_used": exchange_balance["used"],
                "exchange_total": exchange_balance["total"] - balance_per_type.get("margin", 0),
                "exchange_free": exchange_balance["free"] - balance_per_type.get("margin", 0),
                # Disponible sur le compte margin
                "margin_available": balance_per_type.get("margin", 0),
                # Bloqué en position
                "margin_in_position": 0,
                # Emprunté
                "margin_borrowed": -margin_balance.get("amount", 0),
                # Total
                "margin_total": balance_per_type.get("margin", 0) + margin_balance.get("amount", 0),
                "margin_pending_gain": 0,
                "margin_lending_fees": margin_balance.get("lendingFees", 0),
                "margin_pending_base_gain": margin_balance.get("pl", 0),
                "margin_position_type": margin_balance.get("type", None),
                "margin_liquidation_price": margin_balance.get("liquidationPrice", 0),
                "margin_borrowed_base_price": margin_balance.get("total", 0),
                "margin_borrowed_base_currency": margin_balance.get("baseCurrency", None),
                }
            if len(margin_balance) > 0:
                in_positions.setdefault(margin_balance["baseCurrency"], 0)
                in_positions[margin_balance["baseCurrency"]] += margin_balance["total"]

                pending_pl.setdefault(margin_balance["baseCurrency"], 0)
                pending_pl[margin_balance["baseCurrency"]] += margin_balance["pl"]

            # J’emprunte 0.12062983 que je revends à 0.06003598 BTC/DASH, soit 0.00724213 BTC.
            # Sur ces 0.00724213 BTC je récupère 0.00724213*(1-0.0015) = 0.00723127 BTC
            # 
            # -> ordertrades ne tient pas compte des fees
            #     amount = montant vendu (un seul mouvement)
            #     rate = à ce taux
            #     total = total en BTC (pour ce mouvement)
            # -> marginposition:
            #     amount = ce que je dois rendre
            #     basePrice = prix de vente en tenant compte des fees
            #     (amount * basePrice = la quantité de BTC que j’ai effectivement
            #     reçue à erreur d’arrondi près, utiliser plutôt "total")
            #     total = la quantité de BTC que j’ai reçue
            #     pl = plus value actuelle si je rachetais tout de suite
            # -> marginaccountsummary:
            #     currentMargin = La marge actuelle (= netValue/totalBorrowedValue)
            #     totalValue = BTC actuellement en margin (déposé)
            #     totalBorrowedValue = sum (amount * ticker[lowestAsk])
            #     pl = sum(pl)
            #     netValue = BTC actuellement en margin (déposé) + pl
            # Exemple:
            # In [38]: m.ccxt.private_post_returnordertrades({"orderNumber": "XXXXXXXXXXXX"})
            # Out[38]:
            # [{'amount': '0.11882982',
            #   'currencyPair': 'BTC_DASH',
            #   'date': '2018-02-26 22:48:35',
            #   'fee': '0.00150000',
            #   'globalTradeID': 348891380,
            #   'rate': '0.06003598',
            #   'total': '0.00713406',
            #   'tradeID': 9634443,
            #   'type': 'sell'},
            #  {'amount': '0.00180000',
            #   'currencyPair': 'BTC_DASH',
            #   'date': '2018-02-26 22:48:30',
            #   'fee': '0.00150000',
            #   'globalTradeID': 348891375,
            #   'rate': '0.06003598',
            #   'total': '0.00010806',
            #   'tradeID': 9634442,
            #   'type': 'sell'}]
            # 
            # In [51]: m.ccxt.privatePostGetMarginPosition({"currencyPair": "BTC_DASH"})
            # Out[51]:
            # {'amount': '-0.12062982',
            #  'basePrice': '0.05994587',
            #  'lendingFees': '0.00000000',
            #  'liquidationPrice': '0.15531479',
            #  'pl': '0.00000122',
            #  'total': '0.00723126',
            #  'type': 'short'}
            # In [52]: m.ccxt.privatePostGetMarginPosition({"currencyPair": "BTC_BTS"})
            # Out[52]:
            # {'amount': '-332.97159188',
            #  'basePrice': '0.00002171',
            #  'lendingFees': '0.00000000',
            #  'liquidationPrice': '0.00005543',
            #  'pl': '0.00029548',
            #  'total': '0.00723127',
            #  'type': 'short'}
            # 
            # In [53]: m.ccxt.privatePostReturnMarginAccountSummary()
            # Out[53]:
            # {'currentMargin': '1.04341991',
            #  'lendingFees': '0.00000000',
            #  'netValue': '0.01478093',
            #  'pl': '0.00029666',
            #  'totalBorrowedValue': '0.01416585',
            #  'totalValue': '0.01448427'}

        for currency, in_position in in_positions.items():
            all_balances[currency]["total"] += in_position
            all_balances[currency]["margin_in_position"] += in_position
            all_balances[currency]["margin_total"] += in_position

        for currency, pl in pending_pl.items():
            all_balances[currency]["margin_pending_gain"] += pl

        return all_balances

    def order_precision(self, symbol):
        self.load_markets()
        return self.markets[symbol]['precision']['price']

    def transfer_balance(self, currency, amount, from_account, to_account):
        result = self.privatePostTransferBalance({
            "currency": currency,
            "amount": amount,
            "fromAccount": from_account,
            "toAccount": to_account,
            "confirmed": 1})
        return result["success"] == 1

    def close_margin_position(self, currency, base_currency):
        """
        closeMarginPosition({"currencyPair": "BTC_DASH"})
        fermer la position au prix du marché
        """
        symbol = "{}_{}".format(base_currency, currency)
        self.privatePostCloseMarginPosition({"currencyPair": symbol})

    def tradable_balances(self):
        """
        portfolio.market.privatePostReturnTradableBalances()
        Returns tradable balances in margin
        'BTC_DASH': {'BTC': '0.01266999', 'DASH': '0.08574839'},
        Je peux emprunter jusqu’à 0.08574839 DASH ou 0.01266999 BTC (une position est déjà ouverte)
        'BTC_CLAM': {'BTC': '0.00585143', 'CLAM': '7.79300395'},
        Je peux emprunter 7.7 CLAM pour les vendre contre des BTC, ou emprunter 0.00585143 BTC pour acheter des CLAM
        """

        tradable_balances = self.privatePostReturnTradableBalances()
        for symbol, balances in tradable_balances.items():
            for currency, balance in balances.items():
                balances[currency] = decimal.Decimal(balance)
        return tradable_balances

    def margin_summary(self):
        """
        portfolio.market.privatePostReturnMarginAccountSummary()
        Returns current informations for margin
        {'currentMargin': '1.49680968',      -> marge (ne doit pas descendre sous 20% / 0.2)
                                                = netValue / totalBorrowedValue
        'lendingFees': '0.00000000',        -> fees totaux
        'netValue': '0.01008254',           -> balance + plus-value
        'pl': '0.00008254',                 -> plus value latente (somme des positions)
        'totalBorrowedValue': '0.00673602', -> valeur empruntée convertie en BTC.
                (= sum(amount * ticker[lowerAsk]) pour amount dans marginposition)
        'totalValue': '0.01000000'}         -> balance (collateral déposé en margin)
        """
        summary = self.privatePostReturnMarginAccountSummary()

        return {
                "current_margin": decimal.Decimal(summary["currentMargin"]),
                "lending_fees": decimal.Decimal(summary["lendingFees"]),
                "gains": decimal.Decimal(summary["pl"]),
                "total_borrowed": decimal.Decimal(summary["totalBorrowedValue"]),
                "total": decimal.Decimal(summary["totalValue"]),
                }

    def fetch_nth_order_book(self, symbol, action, number):
        book = self.fetch_order_book(symbol, limit=number)
        return decimal.Decimal(book[action + "s"][-1][0])

    def nonce(self):
        """
        Wrapped to allow nonce with other libraries
        """
        return self.nanoseconds()

    def fetch_balance(self, params={}):
        """
        Wrapped to get decimals
        """
        self.load_markets()
        balances = self.privatePostReturnCompleteBalances(self.extend({
            'account': 'all',
            }, params))
        result = {'info': balances}
        currencies = list(balances.keys())
        for c in range(0, len(currencies)):
            id = currencies[c]
            balance = balances[id]
            currency = self.common_currency_code(id)
            account = {
                    'free': decimal.Decimal(balance['available']),
                    'used': decimal.Decimal(balance['onOrders']),
                    'total': decimal.Decimal(0.0),
                    }
            account['total'] = self.sum(account['free'], account['used'])
            result[currency] = account
        return self.parse_balance(result)

    def parse_ticker(self, ticker, market=None):
        """
        Wrapped to get decimals
        """
        timestamp = self.milliseconds()
        symbol = None
        if market:
            symbol = market['symbol']
        return {
            'symbol': symbol,
            'timestamp': timestamp,
            'datetime': self.iso8601(timestamp),
            'high': decimal.Decimal(ticker['high24hr']),
            'low': decimal.Decimal(ticker['low24hr']),
            'bid': decimal.Decimal(ticker['highestBid']),
            'ask': decimal.Decimal(ticker['lowestAsk']),
            'vwap': None,
            'open': None,
            'close': None,
            'first': None,
            'last': decimal.Decimal(ticker['last']),
            'change': decimal.Decimal(ticker['percentChange']),
            'percentage': None,
            'average': None,
            'baseVolume': decimal.Decimal(ticker['quoteVolume']),
            'quoteVolume': decimal.Decimal(ticker['baseVolume']),
            'info': ticker,
        }

    def create_order(self, symbol, type, side, amount, price=None, account="exchange", lending_rate=None, params={}):
        """
        Wrapped to handle margin and exchange accounts, and get decimals
        """
        if type == 'market':
            raise ExchangeError(self.id + ' allows limit orders only')
        self.load_markets()
        if account == "exchange":
            method = 'privatePost' + self.capitalize(side)
        elif account == "margin":
            method = 'privatePostMargin' + self.capitalize(side)
            if lending_rate is not None:
                params = self.extend({"lendingRate": lending_rate}, params)
        else:
            raise NotImplementedError
        market = self.market(symbol)
        response = getattr(self, method)(self.extend({
            'currencyPair': market['id'],
            'rate': self.price_to_precision(symbol, price),
            'amount': self.amount_to_precision(symbol, amount),
        }, params))
        timestamp = self.milliseconds()
        order = self.parse_order(self.extend({
            'timestamp': timestamp,
            'status': 'open',
            'type': type,
            'side': side,
            'price': price,
            'amount': amount,
        }, response), market)
        id = order['id']
        self.orders[id] = order
        return self.extend({'info': response}, order)

    def price_to_precision(self, symbol, price):
        """
        Wrapped to avoid float
        """
        return ('{:.' + str(self.markets[symbol]['precision']['price']) + 'f}').format(price).rstrip("0").rstrip(".")

    def amount_to_precision(self, symbol, amount):
        """
        Wrapped to avoid float
        """
        return ('{:.' + str(self.markets[symbol]['precision']['amount']) + 'f}').format(amount).rstrip("0").rstrip(".")

    def common_currency_code(self, currency):
        """
        Wrapped to avoid the currency translation
        """
        return currency

    def currency_id(self, currency):
        """
        Wrapped to avoid the currency translation
        """
        return currency
