import psycopg2
import redis as _redis

redis = None
psql = None

def redis_connected():
    global redis
    if redis is None:
        return False
    else:
        try:
            return redis.ping()
        except Exception:
            return False

def psql_connected():
    global psql
    return psql is not None and psql.closed == 0

def connect_redis(args):
    global redis

    redis_config = {
            "host": args.redis_host,
            "port": args.redis_port,
            "db": args.redis_database,
            }
    if redis_config["host"].startswith("/"):
        redis_config["unix_socket_path"] = redis_config.pop("host")
        del(redis_config["port"])
    del(args.redis_host)
    del(args.redis_port)
    del(args.redis_database)

    if redis is None:
        redis = _redis.Redis(**redis_config)

def connect_psql(args):
    global psql
    pg_config = {
            "host": args.db_host,
            "port": args.db_port,
            "user": args.db_user,
            "password": args.db_password,
            "database": args.db_database,
            }
    del(args.db_host)
    del(args.db_port)
    del(args.db_user)
    del(args.db_password)
    del(args.db_database)

    if psql is None:
        psql = psycopg2.connect(**pg_config)
