from .helper import limits
from tests.acceptance import AcceptanceTestCase

import unittest
import glob

__all__ = []

for dirfile in glob.glob("tests/acceptance/**/*/", recursive=True):
    json_files = glob.glob("{}/*.json".format(dirfile))
    log_files = glob.glob("{}/*.log".format(dirfile))
    if len(json_files) > 0:
        name = dirfile.replace("tests/acceptance/", "").replace("/", "_")[0:-1]
        cname = "".join(list(map(lambda x: x.capitalize(), name.split("_"))))

        globals()[cname] = unittest.skipUnless("acceptance" in limits, "Acceptance skipped")(
                type(cname, (AcceptanceTestCase, unittest.TestCase), {
                    "log_files": log_files,
                    "files": json_files,
                    "test_{}".format(name): AcceptanceTestCase.base_test
                    })
                )
        __all__.append(cname)


