import sys
import unittest
from decimal import Decimal as D
from unittest import mock
import requests_mock
from io import StringIO
import portfolio, market, main, store, dbs
import datetime

__all__ = ["limits", "unittest", "WebMockTestCase", "mock", "D",
        "StringIO", "tz"]

limits = ["acceptance", "unit"]
for test_type in limits:
    if "--no{}".format(test_type) in sys.argv:
        sys.argv.remove("--no{}".format(test_type))
        limits.remove(test_type)
    if "--only{}".format(test_type) in sys.argv:
        sys.argv.remove("--only{}".format(test_type))
        limits = [test_type]
        break

def tz(hours):
    return datetime.timezone(datetime.timedelta(hours=hours))

class WebMockTestCase(unittest.TestCase):
    import time

    def market_args(self, debug=False, quiet=False, report_path=None, **kwargs):
        return main.configargparse.Namespace(report_path=report_path,
                debug=debug, quiet=quiet, **kwargs)

    def setUp(self):
        super().setUp()
        self.wm = requests_mock.Mocker()
        self.wm.start()

        # market
        self.m = mock.Mock(name="Market", spec=market.Market)
        self.m.debug = False

        self.patchers = [
                mock.patch.multiple(market.Portfolio,
                    data=store.LockedVar(None),
                    liquidities=store.LockedVar({}),
                    last_date=store.LockedVar(None),
                    report=mock.Mock(),
                    worker=None,
                    worker_tag="",
                    worker_notify=None,
                    worker_started=False,
                    poll_started_at=None,
                    callback=None),
                mock.patch.multiple(portfolio.Computation,
                    computations=portfolio.Computation.computations),
                mock.patch.multiple(dbs,
                    redis=None, psql=None)
                ]
        for patcher in self.patchers:
            patcher.start()

    def tearDown(self):
        for patcher in self.patchers:
            patcher.stop()
        self.wm.stop()
        super().tearDown()

